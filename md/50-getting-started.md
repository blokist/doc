## Welcome to BLOKIST

<center>
    <div class="tablet">
        <img style="width:100%" src="img/demo.gif"/>
    </div>
</center>

BLOKIST is a portable rapid prototyping tool for developers and non-developers.
How? By keeping things visual & clickable from the start.

Usecases:

* share prototypes & simulations
* embeddable businesslogic (iframe)
* dynamic JSON configuration & statemachines
* app development (phonegap)
* dashboards (widescreen / BI / BigData)
* education

## Getting started

Eventhough BLOKIST should run fine using touch, an keyboard & mouse is adviced for serious work.
Basically the workflow is:

### 1. open the graph-view

<center>
    <img style="width:100%; max-width:350px;" src="img/graphview.png"/>
</center>

### 2. add nodes (right-click mouse or double-tap)

<center>
    <img style="width:100%; max-width:350px;" src="img/addnode.gif"/>
</center>

### 3. connect by click+drag connectors

<center>
    <img style="width:100%; max-width:350px;" src="img/connect.gif"/>
</center>

> NOTE: there are 2 types of connectors, (JS) events &amp; simple values.

### 4. write javascript (for advanced users)

Extend or create nodes programmatically using javascript:

<center>
    <img style="width:100%; max-width:350px;" src="img/code.gif"/>
</center>

### 4. share or embed!

Press the &lt;&nbsp;&gt; icon in the topbar.

## Tutorials

The best way to start is looking at examples (click book-icon in the topbar, next to the cpu-meters)

